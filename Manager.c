#include <stdbool.h>
#include <stdio.h>

#ifndef UNICODE
# define UNICODE
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <tlhelp32.h>

bool utf16ToUtf8(const wchar_t *source, const int size, char *destination)
{
	if (!WideCharToMultiByte(CP_UTF8, 0, source, -1, destination, size, NULL, NULL)) {
		printf("WideCharToMultiByte() failed with error %d\n", GetLastError());
		return false;
	}

	return true;
}

int main()
{
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap == INVALID_HANDLE_VALUE) {
		printf("CreateToolhelp32Snapshot() failed with error %d\n", GetLastError());
		return 1;
	}

	PROCESSENTRY32 pe;
	pe.dwSize = sizeof(pe);

	BOOL ok = Process32First(hSnap, &pe);
	if (!ok) {
		printf("Process32First() failed with error %d\n", GetLastError());
		return 2;
	}

	char name[MAX_PATH];

	while (ok) {
		if (utf16ToUtf8(pe.szExeFile, sizeof(name), name)) {
			printf("[%u] %s\n", pe.th32ProcessID, name);
		} else {
			printf("utf16ToUtf8() failed, skipping entry...\n");
		}

		ok = Process32Next(hSnap, &pe);
	}

	CloseHandle(hSnap);

	return 0;

}

// new
// #include <windows.h>
// #include <stdio.h>
// #include <tchar.h>
// #include <psapi.h>
// #pragma comment(lib, "psapi.lib")

// // To ensure correct resolution of symbols, add Psapi.lib to TARGETLIBS
// // and compile with -DPSAPI_VERSION=1

// void PrintProcessNameAndID(DWORD processID)
// {
//     TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

//     // Get a handle to the process.

//     HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
//                                       PROCESS_VM_READ,
//                                   FALSE, processID);

//     // Get the process name.

//     if (NULL != hProcess)
//     {
//         HMODULE hMod;
//         DWORD cbNeeded;

//         if (EnumProcessModules(hProcess, &hMod, sizeof(hMod),
//                                &cbNeeded))
//         {
//             GetModuleBaseName(hProcess, hMod, szProcessName,
//                               sizeof(szProcessName) / sizeof(TCHAR));
//         }
//     }

//     // Print the process name and identifier.

//     _tprintf(TEXT("%s  (PID: %u)\n"), szProcessName, processID);

//     // Release the handle to the process.

//     CloseHandle(hProcess);
// }

// int main(void)
// {
//     // Get the list of process identifiers.

//     DWORD aProcesses[1024], cbNeeded, cProcesses;
//     unsigned int i;

//     if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
//     {
//         return 1;
//     }

//     // Calculate how many process identifiers were returned.

//     cProcesses = cbNeeded / sizeof(DWORD);

//     // Print the name and process identifier for each process.

//     for (i = 0; i < cProcesses; i++)
//     {
//         if (aProcesses[i] != 0)
//         {
//             PrintProcessNameAndID(aProcesses[i]);
//         }
//     }

//     return 0;
// }